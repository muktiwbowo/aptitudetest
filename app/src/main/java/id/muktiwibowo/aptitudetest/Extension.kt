package id.muktiwibowo.aptitudetest

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.hideSoftKeyboard() {
    if (this.requestFocus()) {
        val inputMethodManager =
            this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
    }
}

fun yelpRating(rating: Double): Int {
    return if (rating < 1) {
        R.drawable.stars_small_0
    } else if (rating >= 1 && rating < 2) {
        R.drawable.stars_small_1
    } else if (rating >= 2 && rating < 3) {
        R.drawable.stars_small_2
    } else if (rating >= 3 && rating < 4) {
        R.drawable.stars_small_3
    } else if (rating >= 4 && rating < 5) {
        R.drawable.stars_small_4
    } else {
        R.drawable.stars_small_5
    }
}