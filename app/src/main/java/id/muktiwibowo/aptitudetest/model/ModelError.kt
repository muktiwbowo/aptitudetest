package id.muktiwibowo.aptitudetest.model

data class ModelError(
    val code: String?,
    val description: String?
)