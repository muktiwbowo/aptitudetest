package id.muktiwibowo.aptitudetest.model

data class ModelBusiness(
    val total: Int?,
    val businesses: MutableList<Business>?,
    val region: Region?
) {
    data class Business(
        val id: String,
        val alias: String,
        val name: String,
        val image_url: String,
        val is_claimed: Boolean,
        val is_closed: Boolean,
        val url: String,
        val phone: String,
        val display_phone: String,
        val review_count: Int,
        val categories: MutableList<Category>,
        val rating: Double,
        val location: Location,
        val coordinates: Coordinate,
        val photos: MutableList<String>,
        val price: String,
        val hours: MutableList<Hours>?,
        val distance: Double
    ) {
        data class Category(
            val alias: String,
            val title: String
        )

        data class Coordinate(
            val latitude: Double,
            val longitude: Double
        )

        data class Location(
            val city: String,
            val country: String,
            val address2: String,
            val address3: String,
            val state: String,
            val address1: String,
            val zip_code: String,
            val display_address: MutableList<String>
        )

        data class Hours(
            val open: MutableList<Open>,
            val hours_type: String,
            val is_open_now: Boolean
        ) {
            data class Open(
                val is_overnight: Boolean,
                val start: Int,
                val end: Int,
                val day: Int
            )
        }
    }

    data class Region(
        val center: Business.Coordinate
    )
}
