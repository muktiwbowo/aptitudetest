package id.muktiwibowo.aptitudetest.base

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.muktiwibowo.aptitudetest.service.ServiceRetrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BaseModule {
    @Singleton
    @Provides
    fun provideRetrofit(): ServiceRetrofit.API = ServiceRetrofit().initialize()
}