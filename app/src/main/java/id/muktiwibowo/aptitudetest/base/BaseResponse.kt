package id.muktiwibowo.aptitudetest.base

class BaseResponse<out T>(val status: Status, val data: T?, val message: String?) {
    enum class Status {
        LOADING, SUCCESS, ERROR
    }

    companion object {
        fun <T> loading(): BaseResponse<T> {
            return BaseResponse(Status.LOADING, null, null)
        }

        fun <T> success(data: T?): BaseResponse<T> {
            return BaseResponse(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String?): BaseResponse<T> {
            return BaseResponse(Status.ERROR, null, message)
        }
    }
}