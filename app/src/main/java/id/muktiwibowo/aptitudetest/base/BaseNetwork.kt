package id.muktiwibowo.aptitudetest.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.google.gson.Gson
import id.muktiwibowo.aptitudetest.model.ModelError
import kotlinx.coroutines.Dispatchers
import retrofit2.Response

abstract class BaseNetwork {
    protected fun <T> fetch(call: suspend () -> BaseResponse<T>): LiveData<BaseResponse<T>> =
        liveData(Dispatchers.IO) {
            emit(BaseResponse.loading())
            val response = call.invoke()
            if (response.status == BaseResponse.Status.SUCCESS) {
                emit(BaseResponse.success(response.data))
            } else if (response.status == BaseResponse.Status.ERROR) {
                emit(BaseResponse.error(response.message))
            }
        }

    protected suspend fun <T> result(network: suspend () -> Response<T>): BaseResponse<T> {
        try {
            val response = network()
            val body = response.body()
            val error =
                Gson().fromJson(response.errorBody()?.string(), ModelError::class.java)
            if (response.isSuccessful) {
                return if (body != null) BaseResponse.success(body)
                else BaseResponse.error(error?.description)
            }
            return BaseResponse.error(error?.description)
        } catch (e: Exception) {
            return BaseResponse.error(e.localizedMessage ?: e.toString())
        }
    }
}