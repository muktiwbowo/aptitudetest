package id.muktiwibowo.aptitudetest.activity

import android.content.Intent
import android.content.Intent.ACTION_DIAL
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import id.muktiwibowo.aptitudetest.R
import id.muktiwibowo.aptitudetest.base.BaseResponse
import id.muktiwibowo.aptitudetest.databinding.ActivityBusinessDetailBinding
import id.muktiwibowo.aptitudetest.model.ModelBusiness
import id.muktiwibowo.aptitudetest.viewmodel.ViewModelBusiness
import id.muktiwibowo.aptitudetest.yelpRating

@AndroidEntryPoint
class ActivityBusinessDetail : AppCompatActivity() {
    private lateinit var binding: ActivityBusinessDetailBinding
    private lateinit var viewModelBusiness: ViewModelBusiness

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }

    private fun bind() {
        binding = ActivityBusinessDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val businessID = intent.getStringExtra("business_id")

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.toolbar_title_detail)
        }
        viewModelBusiness = ViewModelProvider(this)[ViewModelBusiness::class.java]
        showBusinessDetail(businessID)
    }

    private fun showBusinessDetail(businessID: String?) {
        viewModelBusiness.getBusinessByID(businessID.toString()).observe(this, { response ->
            when (response.status) {
                BaseResponse.Status.LOADING -> {
                    binding.root.isRefreshing = true
                }
                BaseResponse.Status.SUCCESS -> {
                    binding.root.isRefreshing = false

                    val business = response.data
                    business?.let {
                        populateBusiness(business)
                        intentToInformation(
                            business.phone, business.coordinates.latitude,
                            business.coordinates.longitude, business.url
                        )
                    }
                }
                BaseResponse.Status.ERROR -> {
                    binding.root.isRefreshing = false
                    Snackbar.make(binding.root, response.message.toString(), Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        })
    }

    private fun stringToTime(time: String): String {
        return time.chunked(2).toString().replace(",", ":")
            .replace(" ", "")
            .replace("[", "").replace("]", "")
    }

    private fun populateBusiness(business: ModelBusiness.Business) {
        with(binding) {
            tvBusinessName.text = business.name
            val categories = mutableListOf<String>()
            business.categories.forEach { category ->
                categories.add(category.title)
            }
            tvBusinessCategory.text = categories.toString()
                .replace("[", "").replace("]", "")

            if (!business.price.isNullOrEmpty()) tvBusinessPrice.text = business.price
            else tvBusinessPrice.visibility = View.INVISIBLE

            val openOrClosed = if (!business.is_closed) "Open" else "Closed"
            var open = business.hours?.get(0)?.open?.get(0)?.start.toString()
            var closed = business.hours?.get(0)?.open?.get(0)?.end.toString()

            if (open.length == 3) open = String.format("0%s", open)

            if (closed.length == 3) closed = String.format("0%s", closed)

            tvOpenHours.text = String.format(
                "%s %s - %s", openOrClosed,
                stringToTime(open),
                stringToTime(closed)
            )
            tvBusinessRating.text = business.rating.toString()
            Glide.with(root.context).load(yelpRating(business.rating)).into(ivBusinessStars)
            Glide.with(root.context).load(business.image_url).into(ivBusinessCover)
        }
    }

    private fun intentToInformation(
        phoneCall: String,
        latitude: Double,
        longitude: Double,
        website: String
    ) {
        with(binding) {
            ivPhoneCall.setOnClickListener {
                val uri = Uri.parse(String.format("tel:%s", phoneCall))
                startActivity(Intent(ACTION_DIAL).setData(uri))
            }

            ivLocation.setOnClickListener {
                val uri = Uri.parse("geo:$latitude,$longitude")
                val mapIntent = Intent(ACTION_VIEW, uri)
                mapIntent.setPackage("com.google.android.apps.maps")
                startActivity(mapIntent)
            }
            ivWebsite.setOnClickListener {
                val uri = Uri.parse(website)
                startActivity(Intent(ACTION_VIEW, uri))
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}