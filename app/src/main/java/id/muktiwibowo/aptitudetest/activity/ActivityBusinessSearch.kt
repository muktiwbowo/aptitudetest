package id.muktiwibowo.aptitudetest.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import id.muktiwibowo.aptitudetest.R
import id.muktiwibowo.aptitudetest.adapter.AdapterBusiness
import id.muktiwibowo.aptitudetest.base.BaseResponse
import id.muktiwibowo.aptitudetest.databinding.ActivityBusinessSearchBinding
import id.muktiwibowo.aptitudetest.hideSoftKeyboard
import id.muktiwibowo.aptitudetest.viewmodel.ViewModelBusiness

@AndroidEntryPoint
class ActivityBusinessSearch : AppCompatActivity() {
    enum class SearchType {
        LOCATION, NAME, CUISINE
    }

    private lateinit var binding: ActivityBusinessSearchBinding
    private lateinit var adapterBusiness: AdapterBusiness
    private lateinit var viewModelBusiness: ViewModelBusiness
    private var searchType = SearchType.LOCATION
    private var location = "New York"
    private var term: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
        listener()
    }

    private fun bind() {
        binding = ActivityBusinessSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = getString(R.string.toolbar_title_search)
        viewModelBusiness = ViewModelProvider(this)[ViewModelBusiness::class.java]

        val layoutManager = LinearLayoutManager(this)
        adapterBusiness = AdapterBusiness()

        with(binding) {
            rvSearch.apply {
                this.layoutManager = layoutManager
                this.adapter = adapterBusiness
            }
        }
        showBusiness(location)
    }

    private fun listener() {
        adapterBusiness.listenerBusiness = object : AdapterBusiness.ListenerBusiness {
            override fun onClickBusiness(idBusiness: String) {
                startActivity(
                    Intent(
                        this@ActivityBusinessSearch,
                        ActivityBusinessDetail::class.java
                    ).putExtra("business_id", idBusiness)
                )
            }
        }

        with(binding) {
            srlSearch.setOnRefreshListener {
                tvSortBy.text = getString(R.string.label_sort_by)
                tvSearchBy.text = getString(R.string.label_search_by)
                location = "New York"
                searchType = SearchType.LOCATION
                showBusiness(location)
            }

            edtSearch.setOnEditorActionListener(TextView.OnEditorActionListener { value, action, _ ->
                if (action == EditorInfo.IME_ACTION_SEARCH) {
                    root.hideSoftKeyboard()
                    when (searchType) {
                        SearchType.NAME -> {
                            term = value.text.toString()
                            showBusiness(location, term)
                        }
                        SearchType.LOCATION -> {
                            location = value.text.toString()
                            showBusiness(location)
                        }
                        SearchType.CUISINE -> {
                            term = value.text.toString()
                            showBusiness(location, term)
                        }
                    }
                    return@OnEditorActionListener true
                }
                return@OnEditorActionListener false
            })

            tvSearchBy.setOnClickListener {
                val popupMenu = PopupMenu(it.context, it)
                popupMenu.menuInflater.inflate(R.menu.menu_search_by, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    tvSearchBy.text = item.title
                    when (item.itemId) {
                        R.id.menu_location -> {
                            searchType = SearchType.LOCATION
                        }
                        R.id.menu_name -> {
                            searchType = SearchType.NAME
                        }
                        R.id.menu_cuisine -> {
                            searchType = SearchType.CUISINE
                        }
                    }
                    true
                }
                popupMenu.show()
            }

            tvSortBy.setOnClickListener {
                val popupMenu = PopupMenu(it.context, it)
                popupMenu.menuInflater.inflate(R.menu.menu_sort_by, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    tvSortBy.text = item.title
                    when (item.itemId) {
                        R.id.menu_distance -> {
                            showBusiness(location, term, "distance")
                        }
                        R.id.menu_rating -> {
                            showBusiness(location, term, "rating")
                        }
                    }
                    true
                }
                popupMenu.show()
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showBusiness(location: String, term: String? = null, sortBy: String? = null) {
        viewModelBusiness.getBusiness(location, term, sortBy).observe(this, { response ->
            when (response.status) {
                BaseResponse.Status.LOADING -> {
                    binding.srlSearch.isRefreshing = true
                }
                BaseResponse.Status.SUCCESS -> {
                    binding.srlSearch.isRefreshing = false
                    val businesses = response.data?.businesses
                    adapterBusiness.businesses.clear()
                    businesses?.let { adapterBusiness.businesses.addAll(it) }
                    adapterBusiness.notifyDataSetChanged()
                }
                BaseResponse.Status.ERROR -> {
                    binding.srlSearch.isRefreshing = false
                    Snackbar.make(binding.root, response.message.toString(), Snackbar.LENGTH_LONG)
                        .show()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }
}