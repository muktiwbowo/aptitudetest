package id.muktiwibowo.aptitudetest.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.muktiwibowo.aptitudetest.repository.RepositoryBusiness
import javax.inject.Inject

@HiltViewModel
class ViewModelBusiness @Inject constructor(private val repoBusiness: RepositoryBusiness) :
    ViewModel() {
    fun getBusiness(location: String, term: String?, sortBy: String?) =
        repoBusiness.getBusiness(location, term, sortBy)

    fun getBusinessByID(businessID: String) = repoBusiness.getBusinessByID(businessID)
}