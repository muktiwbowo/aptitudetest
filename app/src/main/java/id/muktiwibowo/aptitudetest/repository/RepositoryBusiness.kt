package id.muktiwibowo.aptitudetest.repository

import id.muktiwibowo.aptitudetest.base.BaseNetwork
import id.muktiwibowo.aptitudetest.service.ServiceRetrofit
import javax.inject.Inject

class RepositoryBusiness @Inject constructor(private val api: ServiceRetrofit.API) : BaseNetwork() {
    fun getBusiness(location: String, term: String?, sortBy: String?) = fetch {
        result { api.getBusiness(location, term, sortBy) }
    }

    fun getBusinessByID(businessByID: String) = fetch {
        result { api.getBusinessByID(businessByID) }
    }
}