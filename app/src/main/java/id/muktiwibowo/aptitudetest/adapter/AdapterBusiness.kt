package id.muktiwibowo.aptitudetest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.muktiwibowo.aptitudetest.databinding.HolderBusinessBinding
import id.muktiwibowo.aptitudetest.model.ModelBusiness
import id.muktiwibowo.aptitudetest.yelpRating

class AdapterBusiness : RecyclerView.Adapter<HolderBusiness>() {
    interface ListenerBusiness {
        fun onClickBusiness(idBusiness: String)
    }

    var businesses = mutableListOf<ModelBusiness.Business>()
    var listenerBusiness: ListenerBusiness? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderBusiness {
        val binding = HolderBusinessBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return HolderBusiness(binding = binding, listenerBusiness = listenerBusiness)
    }

    override fun onBindViewHolder(holder: HolderBusiness, position: Int) {
        holder.populate(business = businesses[position])
    }

    override fun getItemCount() = businesses.size

}

class HolderBusiness(
    private val binding: HolderBusinessBinding,
    private val listenerBusiness: AdapterBusiness.ListenerBusiness?
) :
    RecyclerView.ViewHolder(binding.root) {
    fun populate(business: ModelBusiness.Business) {
        with(binding) {
            tvBusinessName.text = business.name
            tvBusinessAddress.text = business.location.display_address.toString()
                .replace("[", "").replace("]", "")
            tvBusinessDistance.text = String.format("%.2f km", business.distance / 1000)
            tvBusinessRating.text = business.rating.toString()
            Glide.with(root.context).load(yelpRating(business.rating)).into(ivBusinessStars)
            Glide.with(root.context).load(business.image_url).into(ivBusinessCover)

            root.setOnClickListener {
                listenerBusiness?.onClickBusiness(idBusiness = business.id)
            }
        }
    }
}