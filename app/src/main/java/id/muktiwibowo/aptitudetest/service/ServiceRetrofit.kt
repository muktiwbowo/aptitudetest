package id.muktiwibowo.aptitudetest.service

import id.muktiwibowo.aptitudetest.model.ModelBusiness
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

class ServiceRetrofit {
    interface API {
        @GET("search")
        suspend fun getBusiness(
            @Query("location") latitude: String,
            @Query("term") term: String?,
            @Query("sort_by") sortBy: String?
        ): Response<ModelBusiness>

        @GET("{business_id}")
        suspend fun getBusinessByID(
            @Path("business_id") business_id: String
        ): Response<ModelBusiness.Business>
    }

    fun initialize(): API {
        val builder = OkHttpClient.Builder().addNetworkInterceptor { chain ->
            val request = chain.request()
            val requestBuilder = request.newBuilder()
            requestBuilder.addHeader(
                "Authorization",
                "Bearer 2Pki6DJ1D0TRoojfNA6S_8Z6HvmnyXApJencuAqF5s2WMabDPyucxf1sH2ImmS23FnYJot0b02OvAxSOTY07Ik8LtUt0Dj8srAmGDf7877UGDeR2MJFcmo9ULpbyYXYx"
            )
            chain.proceed(requestBuilder.build())
        }
        return Retrofit.Builder()
            .baseUrl("https://api.yelp.com/v3/businesses/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(builder.build())
            .build().create(API::class.java)
    }
}